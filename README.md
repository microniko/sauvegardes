Des scripts de sauvegarde
=========================

# Miroir
## Description
Script permettant de faire un miroir « brut » d'une machine.

## Configuration
Créer un fichier `/etc/sauvegardes/miroir.config.sh` :

```
# Adresse email pour recevoir le rapport
EMAIL="sauvegarde@xxxxx.com"

# Nom de la sauvegarde
NOM_SAUVEGARDE="miroir-machine"

# Endroit de la sauvegarde au format hôte::module rsync 
CIBLE="hôte.domaine::module"

# Point de montage local (ce qui est sauvegardé) ** AJOUTER un / à la fin **
SOURCE="/"

# Options de Rsync
# --delete : efface avant le transfert les fichiers qui n'existent pas sur la source.
# --del : La suppression des fichiers du côté récepteur est effectuée incrémentalement au cours du transfert. Ceci est une méthode plus rapide que celles avant ou après le transfert.
# --stats : affiche quelques statistiques de transfert de fichiers
# -a : équivalent à -rlptgoD. C'est un moyen rapide de dire que vous voulez la récursion et de préserver pratiquement tout
# -z : compresse.
# -v : Verbeux
# Exclusions (pas de '' !!) :
EXCLUSIONS="--exclude=/proc --exclude=/sauvegarde* --exclude=/sys --exclude=/tmp --exclude=/dev --exclude=/dev" 

OPTIONS_RSYNC="-avz --stats --del "$EXCLUSIONS


# Log dans logger => /var/log/syslog
LOG=$NOM_SAUVEGARDE

# Fichier de fin de sauvegarde
FICHIER_FIN_SAUVEGARDE=$SOURCE$NOM_SAUVEGARDE"_ok"

# Adresse MAC de la cible
MAC_WOL="XX:XX:XX…"

# Durée d'attente
SLEEP="5m"
```

# Sauvegarde des photos
## Description

## Configuration
Créer le fichier `/etc/sauvegardes/photos.config.sh` :

```
# Adresse email pour recevoir le rapport
EMAIL="sauvegarde@xxxxx.com"

# Nom de la sauvegarde
NOM_SAUVEGARDE="sauvegarde-photos"

# Source au format hôte::module rsync 
SOURCE="machine::module"

# Point de montage local (endroit de la sauvegarde, monté automatiquement au boot de la machine) ** AJOUTER un / à la fin **
CIBLE="/sauvegarde-photos/"

# Options de Rsync
# --delete : efface avant le transfert les fichiers qui n'existent pas sur la source.
# --del : La suppression des fichiers du côté récepteur est effectuée incrémentalement au cours du transfert. Ceci est une méthode plus rapide que celles avant ou après le transfert.
# --stats : affiche quelques statistiques de transfert de fichiers
# -a : équivalent à -rlptgoD. C'est un moyen rapide de dire que vous voulez la récursion et de préserver pratiquement tout
# -z : compresse.
# -v : Verbeux
OPTIONS_RSYNC="-az --stats --del"

# Dixième du temps d'attente (en secondes) pour déclarer que la sauvegarde n'est pas
# possible car le montage de la parition n'a pas été possible (il y a 10 tentatives).
ATTENTE_MONTAGE=60

# Log dans logger => /var/log/syslog
LOG=$NOM_SAUVEGARDE

# Fichier de fin de sauvegarde
FICHIER_FIN_SAUVEGARDE=$CIBLE$NOM_SAUVEGARDE"_ok"
echo $FICHIER_FIN_SAUVEGARDE

# Adresse MAC de la machine à réveiller où son les photos
MAC_CIBLE="xx:xx"
MAC_SOURCE="xx.xx"
```

# Sauvegarde

## Description
Sauvegarde dans une série d'archives au format Tar Bzip2 :
* Dossiers
* MySQL
* PostgreSQL
* Mailman
* Ejabberd (désactivé)
* La liste des paquets deb installés

La sauvegarde se fait dans une partition spécifique.

Il est possible d'exporter la sauvegarde via FTP et/ou partition externe (désactivé)

# Configuration
Dans le fichier `/etc/sauvegardes/sauvegarde.config.sh` :

```
# Adresse email pour recevoir le rapport
EMAIL=""

# Nom de la sauvegarde
NOM_SAUVEGARDE="sauvegarde-xxxx"

# Point de montage de la sauvegarde
SAUVE=/sauvegarde

# Périphérique 
DEV = /dev/sdb1

# Point de montage distant
SAUVEGARDE_DISTANTE=/sauvegarde-distante

# Dixième du temps d'attente (en secondes) pour déclarer que la sauvegarde n'est pas
# possible car le montage de la partion n'a pas été possible (il y a 10 tentatives).
ATTENTE_MONTAGE=60

# Fichier LOG 
LOG=/var/log/sauvegarde/$NOM_SAUVEGARDE.log

# Endroit local de la sauvegarde
CHEMIN_SAUVEGARDE=$SAUVE/$semaine/$date_fichiers

# Endroit distant de la sauvegarde 
CHEMIN_SAUVEGARDE_DISTANTE=$SAUVEGARDE_DISTANTE/$semaine/$date_fichiers

# Options de tar :
#     c = création d'une nouvelle archive
#     p = conserve les permissions
#     j = bzip2
#     z = Gzip
#     v = mode verbeux
#     f = fichier : ...
OPTIONS_TAR=cpjf

# Paramètres Connexion FTP
HOTE='ftp.…'
USER=''
MDP=''

# Sauvegarde MySQL
# L'utilisateur de sauvegarde a les droits SELECT et LOCK TABLES
USER_MYSQL='sauvegardeur'
MDP_MYSQL=''

# Liste des listes à sauvegarder séparées par un espace
LISTES="liste1 liste2'

# Liste des dossiers à sauvegardés
DOSSIERS="/home/user1 /home/user2 /var/www"

# Limite de conservation de la sauvegarde en local
# - Classement par semaine (voir $CHEMIN_SAUVEGARDE)
# %W : numéro de semaine dans l’année (00..53). La semaine commence le Lundi.
LIMITE_LOCAL=semaine`date --date '1 week ago' +%W`       # Il y a 3 semaines 

# Limite de conservation de la sauvegarde sur le FTP
LIMITE_FTP=`date --date '40 days ago' +%Y%m%d`

# Limite de conservation sur sauvegarde distante (partage NFS)
LIMITE_DISTANTE=semaine`date --date '1 weeks ago' +%W`    

# Fichier de fin de sauvegarde
FICHIER_FIN_SAUVEGARDE=$SAUVEGARDE_DISTANTE/$NOM_SAUVEGARDE"_"$date_fichiers"_ok"

# Adresse MAC de la cible
MAC_WOL="XX:XX…"

# Durée d'attente
SLEEP="1m"
```

## Journaux de la sauvegarde
La sauvegarde créé un fichier log dans `$LOG`, afin de ne pas être envahie par les logs, créer une config pour logrotate dans `/etc/logrotate.d/sauvegarde`

```
 /var/log/sauvegarde/*.log {
        weekly
        missingok
        rotate 30
        compress
        delaycompress
        notifempty
        create 640 root adm
        sharedscripts
        postrotate
        endscript
}
```


# Secours
## Description
Sauvegarde brutale vers une machine distante à travers SSH

## Configuration

Créer le fichier de configuration `/etc/sauvegardes/secours.config.sh` :

```
# ---------------------------------------------------------------- #
#                    Variables de configuration                    #

# Adresse email pour recevoir le rapport
EMAIL=""

# Nom de la sauvegarde
NOM_SAUVEGARDE="secours-machine"

# Serveur de secours
SERVEUR_SECOURS="machine.domaine.org"

# Endroit de la sauvegarde au format hôte::module rsync 
CIBLE="user@$SERVEUR_SECOURS::module"

# Clé SSH
CLE_SSH="/user/.ssh/id_rsa"

# Point de montage local (ce qui est sauvegardé) ** AJOUTER un / à la fin **
SOURCE="/"

# Options de Rsync
# -e : encapsulée dans SSH
# --delete : efface avant le transfert les fichiers qui n'existent pas sur la source.
# --del : La suppression des fichiers du côté récepteur est effectuée incrémentalement au cours du transfert. Ceci est une méthode plus rapide que celles avant ou après le transfert.
# --stats : affiche quelques statistiques de transfert de fichiers
# -a : équivalent à -rlptgoD. C'est un moyen rapide de dire que vous voulez la récursion et de préserver pratiquement tout
# -n : (ou --dry-run) mode de test
# -z : compresse.
# -v : Verbeux
# Exclusions (pas de '' !!) :
EXCLUSIONS="--exclude=/proc --exclude=/sauvegarde --exclude=/sys --exclude=/tmp"

#OPTIONS_RSYNC="-rlptDvzn --stats --del -e \"ssh -i $CLE_SSH\" "$EXCLUSIONS
OPTIONS_RSYNC="-aqz --stats --del -e ssh -i $CLE_SSH "$EXCLUSIONS

# Port-knocking : port à frapper avant
PORT_KNOCKING=432


# Log dans logger => /var/log/syslog
LOG=$NOM_SAUVEGARDE

# Fichier de fin de sauvegarde
FICHIER_FIN_SAUVEGARDE=$SOURCE$NOM_SAUVEGARDE"_ok"
```

# Mise en œuvre des sauvegardes
Créer des tâches cron pour planifier régulièrement les sauvegardes. Utiliser le fichier `cron-sauvegardes` comme modèle à placer dans `/etc/cron.d`.


# Arrêt sauvegarde
Script permettant d'arrêter les machines de sauvegarde à la vue.
